#!/bin/sh
echo "Please type the name of the interface you wish to spoof then press enter. Example: eth0"
read i
export macaddr=$(echo '00'$(od -An -N5 -t xC /dev/urandom) | sed -e 's/ /:/g')
echo "Interface: "$i
echo "New Mac Address: "${macaddr}
ip link set dev $i down
ip link set $i address ${macaddr}
ip link set dev $i up
echo "Mac Address Set."
echo $(ip link show | grep link)
exit 1
