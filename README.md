# MAC Spoofer
#### MAC Spoofer is a very simple short and sweet script that allows you to spoof your MAC address automatically across Linux distros. The script will generate a random MAC address for you and then apply it to the interface of your choosing.
---
### How to use...
---
##### 1. Determine which interface you want to spoof
`ip link show`
##### This will show a list of all available network interfaces. Most likely the name you want to spoof will be eth0 or wlan0
##### 2. Clone the repo
`git clone https://gitlab.com/opsecgoy/mac-spoofer.git`
##### 3. Change the directory to where you have cloned the repo
`cd mac-spoofer`
##### 4. Add the 'execute' permission to the script
###### (This is only required the first time you run the script)
`chmod +x macspoofer.sh`
*Note: This may require* `sudo` *depending on your permissions*
##### 5. Finally execute the script
`sudo ./macspoofer.sh`
### Verify
##### The script automatically prints out the result of the command `ip link show | grep link` for you to verify.
---
##### To manually verify your MAC address has changed on the proper interface you can issue the following command
`ip link show`
##### You should see the new MAC address listed under the interface you chose to spoof.
